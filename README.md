Exam Arena Website(Link: https://examarena.herokuapp.com)
## Introduction:

1.This is a website for giving examination for wbjee and jeemain students for Rajarshi Roy's students.  

2.It is also used to communicate with the students by giving them notices and providing pdf files also.  

3.There are also admin section to give Rajarshi Roy the full control of his website.  

4.It also has android application in playstore also.<br/>


## Technology Stack Used:

1.Next.js(server side rendering for react.js)  

2.React.js  

3.Node.js (for backend server)  

4.firebase(Authentication,realtime database,firebase storage)  



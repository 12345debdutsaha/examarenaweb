import React from 'react';
import {Input,InputGroup,Label,Container,Row,Col,Button} from 'reactstrap';
import {fire} from '../../firebase/firebase'
import swal from 'sweetalert';
import Router from 'next/router'
var securityKey="Papadada@983"
const CryptoJS=require('crypto-js')
export default class QuestionFormje extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            text:'',
            Image:'',
            Correct:'',
            progress:false,
            index:'',
            error:null
        }
        this.onSubmit=this.onSubmit.bind(this);
    }
    onSubmit(){
        const {Image,Correct,text}=this.state;
        if( Correct)
        {
            this.setState({error:null})
            let cipherCorrect=CryptoJS.AES.encrypt(Correct,securityKey)
            let ciphertext=CryptoJS.AES.encrypt(text,securityKey)
            let cipherimage=CryptoJS.AES.encrypt(Image,securityKey)
            fire.database().ref(`/questionsjeemain`).push({
                correct:cipherCorrect.toString(),
                questionText:ciphertext.toString(),
                questionimage:cipherimage.toString()
            }).then((snap)=>{
                this.setState({text:''})
                this.setState({Image:''})
                this.setState({Correct:''})
                this.setState({index:''})
            }).catch((err)=>{
                swal({
                    title:'Error',
                    text:`${err.message}`,
                    button:'Cancel!!',
                    icon:'error'
                  })
            })
        }
        else{
            this.setState({error:"Correct and index value are required"})
        }
    }
    onFileUpload(e){
        if(e.target.files[0])
        {
            let file=e.target.files[0];
        let filename=file.name;
        var storageRef=fire.storage().ref(`/images/${filename}`)
        storageRef.put(file).then((snap)=>{
            storageRef.getDownloadURL().then((url)=>{
                this.setState({Image:url})
        }).catch(err=>
            {
                swal({
                    title:'Error',
                    text:`${err.message}`,
                    button:'Cancel!!',
                    icon:'error'
                  })
            })
    })
}      
}
    render()
    {
        return(
        <div className="question-input">
            <Container>
                <br/>
                        <Label className="label-text">Question Text</Label>
                        <Input value={this.state.text} type="textarea" onChange={(e)=>{this.setState({text:e.target.value})}}/>                    
                        <Label className="label-text">Question Image</Label>
                        <br/>
                        <label className="file-text">Choose file
                        <input type="file" className="file-input"  onChange={(e)=>{this.onFileUpload(e)}}/></label>
                        <br/>
                        <Label className="label-text">Correct answer</Label>
                        <Input value={this.state.Correct} type="textarea" onChange={(e)=>{this.setState({Correct:e.target.value})}}/>                    
                    <br/>
                {this.state.error && <Row className="question-upload" style={{paddingBottom:30}}>
                    <p style={{fontSize:20,color:'#ff6666',fontWeight:'bold'}}>{this.state.error}</p>
                </Row>}
                <br/>
                    <Button onClick={()=>{this.onSubmit()}} color="success">Submit</Button>
            </Container>
        </div>
        );
    }
}
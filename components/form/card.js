import React from 'react';
import {Input ,Button,Tooltip,Form} from 'reactstrap';
import { fire } from '../../firebase/firebase';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import Router from 'next/router';
import { toast } from 'react-toastify';
export default class Card extends React.Component{

    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.togglePassword=this.togglePassword.bind(this);
        this.state = {
          tooltipOpen: false,
          tooltippassword:false,
          isHovered:false,
          useremail:'',
          password:'',
          username:'',
          isloading:false,
        };
      }
      login(){
        const {useremail,password,username,isloading}=this.state;
        this.setState({isloading:true})
        fire.auth().signInWithEmailAndPassword(useremail,password)
        .then(async(user)=>{
          await fire.auth().currentUser.getIdToken(true).then(res=>{
            Cookies.set("jwt",res)            
          }).catch(err=>{
            toast.warn("error occured")
          })
          if(user)
          {
            Cookies.set("user",user.user.uid)
            swal({
              title:'Congratulation!',
              text:'You have just signed in',
              button:'Okk!!',
              icon:'success'
            })
            Router.push('/profile')
          }
          else{
            swal({
              title:'Error',
              text:'Unknown Error Occured',
              button:'Cancel!!',
              icon:'error'
            })
            this.setState({isloading:false})
          }
        }).catch((err)=>{
          swal({
            title:'Error',
            text:`${err.message}`,
            button:'Cancel!!',
            icon:'error'
          })
          this.setState({isloading:false})
        })
      }
      toggle() {
        this.setState({
          tooltipOpen: !this.state.tooltipOpen
        });
      }
      togglePassword() {
        this.setState({
          tooltippassword: !this.state.tooltippassword,
        });
      }

    render(){
      const {isloading}=this.state;
      if(isloading)
      {
        return(
              <div className="loading-div">
                  <p className="loading-title">Verifying Your Data.......</p>
              </div>
        );
      }
        return(
            <div>
            <Form>
                <div className="card-style box effect2">
                <div className="signin-style">
                    <p className="text-style1">Signin With Credential</p>
                </div>
                    <div className="input-style">
                        <p className="text-style2">User Email</p>
                        <Input type="email" id="TooltipExample" className="input-focus" autoComplete="on"
                        onChange={(e)=>{
                          this.setState({useremail:e.target.value});
                        }}
                        />
                        <Tooltip placement="bottom" isOpen={this.state.tooltipOpen} target="TooltipExample" toggle={this.toggle}>
                            Enter a valid email please
                      </Tooltip>
                    </div>
                    <div className="input-style1">
                    <p className="text-style2">User Password</p>
                    <Input type="password" id="Toolpassword" className="input-focus" autoComplete="off"
                    onChange={(e)=>{this.setState({password:e.target.value})}}
                    onKeyDown={(e)=>{
                      if(e.keyCode===13)
                      {
                        this.login()
                      }
                    }}
                    />
                    <Tooltip placement="bottom" isOpen={this.state.tooltippassword} target="Toolpassword" toggle={this.togglePassword}>
                    Enter whatever you have entered
                    </Tooltip>
                    </div>
                    <div className="wrapper1" onClick={()=>{
                      this.login();
                    }}>
                    <a href="#" className="button">Login</a>
                  </div>
                </div>
                </Form>
            </div>
        );
    }
}
import React from 'react'
import Head from 'next/head'
export default class Home extends React.Component{
    render()
    {
        const {isAuthenticated,notice}=this.props;
        return(
          <React.Fragment>
            <Head>
                  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
                  <link rel="stylesheet" href="static/css/linearicons.css" />
                  <link rel="stylesheet" href="static/css/font-awesome.min.css" />
                  <link rel="stylesheet" href="static/css/magnific-popup.css" />
                  <link rel="stylesheet" href="static/css/owl.carousel.css" />
                  <link rel="stylesheet" href="static/css/nice-select.css"/>
                  <link rel="stylesheet" href="static/css/hexagons.min.css" />
                  <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
                  <link rel="stylesheet" href="static/css/main.css" />
                  <script src="static/js/vendor/jquery-2.2.4.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
                      crossOrigin="anonymous"></script>
                    <script src="static/js/vendor/bootstrap.min.js"></script>
                    <script src="static/js/jquery.ajaxchimp.min.js"></script>
                    <script src="static/js/jquery.magnific-popup.min.js"></script>
                    <script src="static/js/parallax.min.js"></script>
                    <script src="static/js/owl.carousel.min.js"></script>
                    <script src="static/js/jquery.sticky.js"></script>
                    <script src="static/js/hexagons.min.js"></script>
                    <script src="static/js/jquery.counterup.min.js"></script>
                    <script src="static/js/waypoints.min.js"></script>
                    <script src="static/js/jquery.nice-select.min.js"></script>
                    <script src="static/js/main.js"></script>
            </Head>
            <section className="home-banner-area">
          <div className="container">
            <div className="row justify-content-center fullscreen align-items-center">
                <div className="col-lg-5 col-md-8 home-banner-left">
          <h1 className="text-white">
            Take the first step <br />
            to learn with us
          </h1>
          <p className="mx-auto text-white  mt-20 mb-40">
            In the history of modern astronomy, there is probably no one
            greater leap forward than the building and launch of the space
            telescope known as the Hubble.
          </p>
        </div>
        <div className="offset-lg-2 col-lg-5 col-md-12 home-banner-right">
          <img className="img-fluid" src="static/img/header-img.png" alt="" />
        </div>
      </div>
    </div>
  </section>
  <section className="feature-area">
    <div className="container-fluid">
      <div className="feature-inner row">
        <div className="col-lg-2 col-md-6">
          <div className="feature-item d-flex">
            <i className="ti-book text-color" ></i>
            <div className="ml-20">
              <h4 className="text-color">New classNamees</h4>
              <p>
                In the history of modern astronomy, there is probably no one greater leap forward.
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-2 col-md-6">
          <div className="feature-item d-flex">
            <i className="ti-cup text-color"></i>
            <div className="ml-20">
              <h4 className="text-color">Top Courses</h4>
              <p>
                In the history of modern astronomy, there is probably no one greater leap forward.
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-2 col-md-6">
          <div className="feature-item d-flex border-right-0">
            <i className="ti-desktop text-color"></i>
            <div className="ml-20">
              <h4 className="text-color">Full E-Books</h4>
              <p>
                In the history of modern astronomy, there is probably no one greater leap forward.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section className="video-area section-gap-bottom">
    <div className="container">
      <div className="row align-items-center row">
        <div className="col-sm" style={{marginTop:20}}>
          <div className="section-title text-white">
            <h2 className="text-white">
              Watch Our Trainers <br/>
              in Live Action
            </h2>
            <p>
              In the history of modern astronomy, there is probably no one greater leap forward than the building and
              launch of the space telescope known as the Hubble.
            </p>
          </div>
        </div>
        <div className="col-sm">
        <img src="static/img/blog-post/b1.jpg" className="image-fix"/>
        </div>
      </div>
    </div>
  </section>
  <section className="other-feature-area">
    <div className="container">
      <div className="feature-inner row">
        <div className="col-lg-12">
          <div className="section-title text-left">
            <h2 className="text-color">
              Features That <br />
              Can Avail By Everyone
            </h2>
          </div>
        </div>
        <div className="col-lg-4 col-md-6">
          <div className="other-feature-item">
            <i className="ti-key"></i>
            <h4>Lifetime Access</h4>
            <div>
                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed
                do eiusmod tempor incididunt labore. Lorem ipsum dolor sit
                amet consec tetur adipisicing elit, sed do eiusmod tempor
                incididunt labore.
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 ">
          <div className="other-feature-item">
            <i className="ti-files"></i>
            <h4>Source File Included</h4>
            <div>
                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed
                do eiusmod tempor incididunt labore. Lorem ipsum dolor sit
                amet consec tetur adipisicing elit, sed do eiusmod tempor
                incididunt labore.
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6">
          <div className="other-feature-item">
            <i className="ti-medall-alt"></i>
            <h4>Student Membership</h4>
            <div>
                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed
                do eiusmod tempor incididunt labore. Lorem ipsum dolor sit
                amet consec tetur adipisicing elit, sed do eiusmod tempor
                incididunt labore.
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6">
          <div className="other-feature-item">
            <i className="ti-briefcase"></i>
            <h4>35000+ Courses</h4>
            <div>
                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed
                do eiusmod tempor incididunt labore. Lorem ipsum dolor sit
                amet consec tetur adipisicing elit, sed do eiusmod tempor
                incididunt labore.
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>  
  <section className="registration-area">
    <div className="container">
      <div className="row align-items-end">
        <div className="col-lg-5">
          <div className="section-title text-left text-white">
            <h2 className="text-white">
              Watch Our Trainers <br/>
              in Live Action
            </h2>
            <p>
              If you are looking at blank cassettes on the web, you may be
              very confused at the difference in price. You may see some for
              as low as $.17 each.
            </p>
          </div>
        </div>
        <div className="offset-lg-3 col-lg-4 col-md-6">
          <div className="course-form-section">
            <h3 className="text-white">Courses for Free</h3>
            <p className="text-white">It is high time for learning</p>
            <form className="course-form-area contact-page-form course-form text-right" id="myForm" action="mail.html" method="post">
              <div className="form-group col-md-12">
                <input type="text" className="form-control" id="name" name="name" placeholder="Name"
                 onBlur={(e)=>{
                   e.target.placeholder='Name'
                 }}/>
              </div>
              <div className="form-group col-md-12">
                <input type="tel" className="form-control" id="subject" name="subject" placeholder="Phone Number"
                 onBlur={(e)=>{
                  e.target.placeholder='Phone Number'                   
                 }}/>
              </div>
              <div className="form-group col-md-12">
                <input type="email" className="form-control" id="email" name="email" placeholder="Email Address"
                 onBlur={(e)=>{
                  e.target.placeholder='Email Address'
                 }}/>
              </div>
              <div className="col-lg-12 text-center">
                <button className="btn text-uppercase">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer className="footer-area section-gap">
		<div className="container">
			<div className="row">
				<div className="col-lg-2 col-md-6 single-footer-widget">
					<h4>Top Products</h4>
					<ul>
						<li><a href="#">Managed Website</a></li>
						<li><a href="#">Manage Reputation</a></li>
						<li><a href="#">Power Tools</a></li>
						<li><a href="#">Marketing Service</a></li>
					</ul>
				</div>
				<div className="col-lg-2 col-md-6 single-footer-widget">
					<h4>Quick Links</h4>
					<ul>
						<li><a href="#">Jobs</a></li>
						<li><a href="#">Brand Assets</a></li>
						<li><a href="#">Investor Relations</a></li>
						<li><a href="#">Terms of Service</a></li>
					</ul>
				</div>
				<div className="col-lg-2 col-md-6 single-footer-widget">
					<h4>Features</h4>
					<ul>
						<li><a href="#">Jobs</a></li>
						<li><a href="#">Brand Assets</a></li>
						<li><a href="#">Investor Relations</a></li>
						<li><a href="#">Terms of Service</a></li>
					</ul>
				</div>
				<div className="col-lg-2 col-md-6 single-footer-widget">
					<h4>Resources</h4>
					<ul>
						<li><a href="#">Guides</a></li>
						<li><a href="#">Research</a></li>
						<li><a href="#">Experts</a></li>
						<li><a href="#">Agencies</a></li>
					</ul>
				</div>
				<div className="col-lg-4 col-md-6 single-footer-widget">
					<h4>Newsletter</h4>
					<p>You can trust us. we only send promo offers,</p>
					<div className="form-wrap" id="mc_embed_signup">
						<form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
						 method="get" className="form-inline">
							<input className="form-control" name="EMAIL" placeholder="Your Email Address"
							 required="" type="email"/>
							<button className="click-btn btn btn-default text-uppercase">subscribe</button>
							<div style={{position:'absolute',left:-5000}}>
								<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabIndex="-1" type="text"/>
							</div>
							<div className="info"></div>
						</form>
					</div>
				</div>
			</div>
			<div className="footer-bottom row align-items-center">
				<p className="footer-text m-0 col-lg-8 col-md-12">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i className="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
 Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div className="col-lg-4 col-md-12 footer-social">
					<a href="#"><i className="fa fa-facebook"></i></a>
					<a href="#"><i className="fa fa-twitter"></i></a>
					<a href="#"><i className="fa fa-dribbble"></i></a>
					<a href="#"><i className="fa fa-behance"></i></a>
				</div>
			</div>
		</div>
	</footer>
  </React.Fragment>

        );
    }
}
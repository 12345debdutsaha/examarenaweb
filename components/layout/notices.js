import React from 'react'
import 
{Card,
CardBody,
CardText,
CardHeader,
CardFooter,
Button
} from 'reactstrap';
import { fire } from '../../firebase/firebase';
import { toast } from 'react-toastify';
export default class Notice extends React.Component{
     handleClick=async()=>{
        if(this.props.id)
        {
            await fire.database().ref('/noticeboard/'+this.props.id).remove().then(()=>{
                toast.success("Successfully deleted")
            }).catch(err=>{
                toast.error("Something goes wrong")
            })
        }else
        {
            toast.error("Yoou are not authorized to delete data")
        }
    }
    render(){
        return(
            <Card className="notice-card">
                <CardHeader style={{color:'white'}}>
                    Notice {this.props.title}
                </CardHeader>
                <CardBody>
                    <CardText style={{color:'white'}}>
                        {this.props.text}
                        {
                            this.props.docs &&
                            <a href={this.props.docs}>Download pdf</a>
                        }
                    </CardText>
                </CardBody>
                <CardFooter style={{color:'white'}}>
                        {this.props.date &&
                        <p>{this.props.date}</p>
                        }
                    {(this.props.type!=='user')?<Button className="btn btn-warning" onClick={this.handleClick}>Delete</Button>:<p></p>}
                </CardFooter>
            </Card>
        );
    }
}
import React from 'react'
import {Card,CardBody,CardText,CardFooter,CardHeader} from 'reactstrap'
import { fire } from '../../firebase/firebase';
import { toast } from 'react-toastify';
export default class DetailCard extends React.Component{
    constructor(props)
    {
        super(props)
        this.handleClick=this.handleClick.bind(this)
    }
    async handleClick(){
        if(this.props.type==='wb')
        {
            await fire.database().ref(`/questionswbjee/${this.props.id}`).remove().then(()=>{
                toast.success("Successfully deleted "+this.props.questionText.slice(0,6)+".....")
            }).catch(err=>{
                toast.error("Something goes wrong")
            })
        }
        else{
            await fire.database().ref(`/questionsjeemain/${this.props.id}`).remove().then(()=>{
                toast.success("Successfully deleted "+this.props.questionText.slice(0,6)+".....")
            }).catch(err=>{
                toast.error("Something goes wrong")
            })
        }
    }
    render()
    {
        const {questionText,questionImage,Correct,index,type}=this.props;
        return(
            <Card className="card-detail1">
            <CardHeader className="card-header1">
                <CardText>
                <p className="text-style1">Question {index}</p>
                </CardText>
            </CardHeader>
                <CardBody className="card-body1">
                    <CardText>
                    <pre className="breakword">{questionText}</pre>
                    </CardText>
                    <CardText>
                    <img src={questionImage}/>
                    </CardText>
                    </CardBody>
                    <CardFooter className="card-footer1">
                    <p className="text-style1">Correct Option: {Correct}</p>
                    <button className="btn btn-warning" onClick={this.handleClick}>Delete</button>
                    </CardFooter>
            </Card>
        );
    }
}
import React from 'react'
import Head from 'next/head'
import $ from 'jquery'
import {Link} from '../../routes'; 
import auth0 from '../../services/auth0';

const Logout=()=>{
    return(
      <span onClick={auth0.logout} className="nav-link port-navbar-link clickable">Logout</span>
    );
  }
export default class HeaderNew extends React.Component{
    constructor(props)
    {
        super(props)
        this.state={
            isOpen:false
        }
    }
    componentDidMount()
    {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            $(this).toggleClass('active');
        });
    }
    render()
    {
    const {isAuthenticated,className,isSiteOwner}=this.props;
        return (
        <React.Fragment>
        <Head>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossOrigin="anonymous"/>            
            <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
            <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
            <link href="static/css/navstyle.css" rel="stylesheet"></link>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        </Head>
        <div className="header-new-container">
        <div className="wrapper">
        <nav id="sidebar">
            <div className="sidebar-header" style={{display:'flex',justifyContent:'center',alignSelf:'center',flexDirection:'column'}}>
            <img src="static/images/ExamArena.png" style={{display:'flex',height:100,width:100,borderRadius:50,alignSelf:'center'}}/>
            <Link href='/'>
			  <a className="nav-link port-navbar-link color-white"><h3 style={{color:'white'}}>Exam Arena</h3></a>
            </Link>
            </div>

            <ul className="list-unstyled components">
                <p className="header-new-p">Rajarshi Roy</p>
                <li>
                <Link href="/">
                    <a className="header-new-a">Home</a>
                    </Link>
                </li>
                {!isAuthenticated && <li>
                    <Link href="/login">
                    <a className="header-new-a">Login</a>
                    </Link>
                </li>}

                {isAuthenticated && <li>
                    <Link href="/profile">
                    <a className="header-new-a">Profile</a>
                    </Link>
                </li>}

                {isAuthenticated && <li>
                    <Link href="/wbjeeexam">
                    <a className="header-new-a">Wbjee Exam</a>
                    </Link>
                </li>}

                {isAuthenticated && <li>
                    <Link href="/jeemain">
                    <a className="header-new-a">Jeemain Exam</a>
                    </Link>
                </li>}
                {isSiteOwner &&<li className="active">
                    <a href="#homeSubmenu" onClick={(e)=>{
                        this.setState({isOpen:!this.state.isOpen})
                    }} data-toggle="collapse" aria-expanded="true" className="dropdown-toggle header-new-a">Admin</a>
                    {this.state.isOpen && <ul className="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <Link route="/idProvide"><a className="header-new-a">Id Providation</a></Link>
                        </li>
                        <li>
                            <Link route="/questionUpload"><a className="header-new-a">Question Upload</a></Link>
                        </li>
                        <li>
                            <Link route="/questionDashBoard"><a className="header-new-a">All Questions</a></Link>
                        </li>
                        <li>
                            <Link route="/noticeUpload"><a className="header-new-a">Notice Upload</a></Link>
                        </li>
                        <li>
                            <Link route="/allnotices"><a className="header-new-a">All Notices</a></Link>
                        </li>
                        <li>
                            <Link route="/permission"><a className="header-new-a">Permission</a></Link>
                        </li>
                    </ul>}
                </li>}
                {isAuthenticated && <li>
                    <Link href="/login">
                        <Logout/>
                    </Link>
                </li>}
            </ul>

            <ul className="list-unstyled CTAs">
                <li>
                    <a onClick={()=>{
                        window.open("https://play.google.com/store/apps/details?id=com.rajorshi.examapp","_newtab")
                    }} className="download" style={{color:'#7386D5',cursor:'pointer'}}>Get Android App</a>
                </li>
            </ul>
        </nav>
        <div id="content">

            <nav className="navbar navbar-expand-lg" style={{backgroundColor:'transparent'}}>
                <div>

                    <button type="button" id="sidebarCollapse" className="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </nav>
            <div>
            {this.props.children}
            </div>
        </div>
        </div>
        </div>
        </React.Fragment>
        );
    }
}
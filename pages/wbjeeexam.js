import React from 'react';
import BaseLayout from '../components/layout/BaseLayout';
import {Card,CardBody,CardFooter,CardText,CardHeader,Button,Container,Row,Col} from 'reactstrap';
import User from '../components/user';
import { fire } from '../firebase/firebase';
import Timer from '../components/timer';
import * as Cookies from 'js-cookie';
import {toast} from 'react-toastify'
import Router from 'next/router'
var secretKey="Papadada@983";
const CryptoJS=require('crypto-js');
import $ from 'jquery'
export default class WbjeeExam extends React.Component{
	constructor(props){
		super(props)
		this.state={
            signup:false,
            user:{},
            isloading:true,
            button:[],
            index:0,
            question:[],
            answer:[],
            abool:false,
            bbool:false,
            cbool:false,
            dbool:false,
            correct:'',
            correctarray:[],
            loadingtext:'Wait a while.....',
            questionkey:[],
            length:0,
            count:0
        }
        this.onChange=this.onChange.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.onFinalSubmit=this.onFinalSubmit.bind(this);
        this.handleTabChange=this.handleTabChange.bind(this)
    }
    onChange(i)
    {
        const {questionkey}=this.state;
        if(i==-1)
        {
            toast.warn("You have reached at the begining");
        }
        else if(i==questionkey.length)
        {
            toast.warn("You have reached the end of your question series")
        }
        else
        this.setState({index:i})
    }
    timerset(){
        setTimeout(this.onFinalSubmit,10800000);        
    }
    handleTabChange()
    {
        if(this.state.count===3)
        {
            this.onFinalSubmit()
        }else
        {
            if(document.visibilityState==='hidden')
            {
                this.state.count+=1
                alert("Please do not switch between tabs")
            }
        }
       
    }
    disableRightClick=(e)=>{
            e.preventDefault();
            return false
    }
    componentDidMount()
    {
        $('body').bind('cut copy paste',(e)=>{
            e.preventDefault();
            return false;
        })

        document.addEventListener("visibilitychange",this.handleTabChange);
        document.addEventListener("contextmenu",this.disableRightClick);
    }
    componentWillUnmount()
    {
        document.removeEventListener("visibilitychange",this.handleTabChange)
        document.removeEventListener("contextmenu",this.disableRightClick);
    }
    async componentWillMount(){
        const {button}=this.state;
        var count=51;
        let question=[];
        var permission=await fire.database().ref('/permissionwbjee').once('value').then((snap)=>{
            var val=snap.val();
            return val.permission;
        }).catch((err)=>{
            toast.error(err.message)
        })
        if(permission)
        {
        question=await fire.database().ref('/questionswbjee').once('value')
        .then((snap)=>{return (snap.val())})
        .catch((err)=>{console.log(err)})
        for(let i in question)
        {
            this.state.questionkey.push(i)
            this.state.length+=1
            var bytesText  = await CryptoJS.AES.decrypt(question[i].questionText.toString(), secretKey);
            question[i].questionText= await bytesText.toString(CryptoJS.enc.Utf8);
            var bytesImage  =await CryptoJS.AES.decrypt(question[i].questionimage.toString(), secretKey);
            question[i].questionimage=await bytesImage.toString(CryptoJS.enc.Utf8);
            var bytesCorrect  = await CryptoJS.AES.decrypt(question[i].correct.toString(), secretKey);
            question[i].correct=await bytesCorrect.toString(CryptoJS.enc.Utf8);
        }
        this.setState({question})
        for(let i=0;i<this.state.length;i++)
        {
            if(i>-1 && i<9)
            {
                if(i%5==0)
                {
                    button.push(<br key={count}/>)
                    button.push(<Button key={i+1} onClick={()=>{this.onChange(i)}} className="jump-button">{0}{i+1}</Button>)
                }
                else{
                    button.push(<Button key={i+1} onClick={()=>{this.onChange(i)}} className="jump-button">{0}{i+1}</Button>)
                }
            }
            else
            {
                if(i%5==0)
                {
                    button.push(<br key={count}/>)
                    button.push(<Button key={i+1} onClick={()=>{this.onChange(i)}} className="jump-button">{i+1}</Button>)
                }
                else
                button.push(<Button key={i+1} onClick={()=>{this.onChange(i)}} className="jump-button">{i+1}</Button>) 
            }
            count+=1;
        }
        this.setState({isloading:false})
    }else{
        this.setState({loadingtext:"You have no permission to enter the test"})
    }
    }

    handleChange(e){
        var name=e.target.name
        if(name==='a')
        {
            this.setState({abool:true})
            this.setState({bbool:false})
            this.setState({cbool:false})
            this.setState({dbool:false})
        }
        else if(name==='b')
        {
            this.setState({bbool:true})
            this.setState({abool:false})
            this.setState({cbool:false})
            this.setState({dbool:false})
        }
        else if(name==='c')
        {
            this.setState({cbool:true})
            this.setState({bbool:false})
            this.setState({abool:false})
            this.setState({dbool:false})
        }
        else if(name==='d')
        {
            this.setState({dbool:true})
            this.setState({bbool:false})
            this.setState({cbool:false})
            this.setState({abool:false})
        }
        this.setState({correct:name})
    }

    onSubmit(){
        const {abool,bbool,cbool,dbool}=this.state;
        if(abool || bbool || cbool || dbool)
        {
            this.state.correctarray[this.state.index]=this.state.correct;
        }else{
            this.state.correctarray[this.state.index]=null;
        }
        this.setState({dbool:false})
        this.setState({bbool:false})
        this.setState({cbool:false})
        this.setState({abool:false})
        console.log(this.state.correctarray);
    }

    onFinalSubmit(){
        const {question,correctarray}=this.state;
        var value=0;
        var index=0
        for(let i in question)
        {
            if(question[i].correct===correctarray[index])
            {
                value=value+2;
            }
            else if(correctarray[index]==undefined || correctarray[index]==null)
            {
                value=value+0
            }
            else{
                value=value-0.5;
            }
            index+=1
        }
        var outof=index*2;
        Cookies.set('userscorewbjee',value);
        Cookies.set('Outofwbjee',outof);
        toast.success("Your score is submitted")
        Router.push('/rating')
    }

	render()
	{
        const {isAuthenticated}=this.props.auth;
        const {isloading,question,index,questionkey}=this.state;
        const {button}=this.state;
        const {correctarray}=this.state;
        if(!isAuthenticated)
        {
            return(
                <User auth={this.props.auth}/>
            );
        }
        if(isloading)
        {
            return(
                <BaseLayout {...this.props.auth}
                title="Exam Arena">
                    <div className="loading-div">
                        <p className="loading-title">{this.state.loadingtext}</p>
                    </div>
              </BaseLayout>	
            );
        }
		return(
			<BaseLayout {...this.props.auth}
            title="Exam Arena Wbjee Examination">
            {this.timerset()}
                {!isloading && <div style={{paddingTop:150,display:'flex',justifyContent:'space-between',alignSelf:'center'}}>
                    <div style={{marginLeft:20}}>
                        <Timer/>
                    </div>
                    <Button color="success" style={{marginRight:20}} onClick={()=>{this.onFinalSubmit()}}>Final Submission</Button>
                </div>}
                <Container>
                    <Row>
                        <Col md="8">
                        <div className="div-pad">
                        <div className="wbjee-div">
                            <Card className="wbjee-card">
                                <CardHeader className="wbjee-title">
                                    Wbjee Examination
                                </CardHeader>
                                <CardBody className="wbjee-body">
                                {question[questionkey[index]].questionText && <CardText>
                                    <pre className="breakword">
                                {question[questionkey[index]].questionText}</pre>
                                </CardText>}
                                {question[questionkey[index]].questionimage && <CardText>
                                <img src={question[questionkey[index]].questionimage} alt="question"/>
                                </CardText>}
                                </CardBody>
                                <CardFooter className="wbjee-footer">
                                    <CardText>
                                        please select a one correct option
                                    </CardText>
                                </CardFooter>
                            </Card>
                        </div>
                        <div className="wbjee-option-div">
                            <Card className="wbjee-option">
                                <CardBody className="wbjee-option-body">
                                <label className="container1 wbjee-option-text">Option a
                                <input type="checkbox" checked={this.state.abool} className="input1" name="a" onChange={(e)=>{this.handleChange(e)}}/>
                                <span className="checkmark"></span>
                              </label>
                                </CardBody>
                            </Card>
                        </div>
                        <div className="wbjee-option-div">
                        <Card className="wbjee-option">
                            <CardBody>
                            <label className="container1 wbjee-option-text">Option b
                            <input type="checkbox" className="input1" checked={this.state.bbool} name="b" onChange={(e)=>{this.handleChange(e)}}/>
                            <span className="checkmark"></span>
                          </label>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="wbjee-option-div">
                    <Card className="wbjee-option">
                        <CardBody>
                        <label className="container1 wbjee-option-text">Option c
                        <input type="checkbox" className="input1" name="c" checked={this.state.cbool} onChange={(e)=>{this.handleChange(e)}}/>
                        <span className="checkmark"></span>
                      </label>
                        </CardBody>
                    </Card>
                </div>
                <div className="wbjee-option-div">
                <Card className="wbjee-option">
                    <CardBody>
                    <label className="container1 wbjee-option-text">Option d
                    <input type="checkbox" className="input1" name="d" checked={this.state.dbool} onChange={(e)=>{this.handleChange(e)}}/>
                    <span className="checkmark"></span>
                  </label>
                    </CardBody>
                </Card>
            </div>
            <div>
                <p className="correct-text">{correctarray[index]}</p>
            </div>
                <div className="Wbjee-button">
                    <Button className="button-prev" onClick={()=>{this.onChange(this.state.index-1)}}>Prev</Button>
                    <Button className="button-next" onClick={()=>{this.onChange(this.state.index+1)}}>Next</Button>
                    <Button className="button-next" onClick={()=>{this.onSubmit()}}>Submit</Button>
                </div>
                    </div>
                        </Col>
                        <Col md="4">
                        <div className="div-pad">
                            <div className="wbjee-jump-box">
                                <div className="jump-buttons">
                                {button}
                                </div>
                            </div>
                        </div>
                    </Col>
                    </Row>
                </Container>
		  </BaseLayout>		  
		);
	}
}
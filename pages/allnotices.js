import React from 'react';
import BaseLayout from '../components/layout/BaseLayout';
import User from '../components/user';
import { fire } from '../firebase/firebase';
import swal from 'sweetalert';
import {Button} from 'reactstrap';
import { toast } from 'react-toastify';
import Notice from '../components/layout/notices';
export default class QusetionDashBoard extends React.Component{
    constructor(props)
    {
        super(props)
        this.state={
            loading:true,
            notice:[],
            noticekey:[]
        }
    }
    async componentWillMount()
    {
        this.setState({loading:true})
        let result=await fire.database().ref('/noticeboard').once('value').then(result=>{
            return result.val()
        }).catch(err=>{
            return null
        })
        if(result!==null)
        {
            var notice=[]
            for(let i in result)
            {
                this.state.noticekey.push(i)
                notice.push(result[i])
            }
        this.setState({notice:notice})
        }else{
            toast.error("Something went wrong")
        }
        this.setState({loading:false})
    }
	render()
	{
        const {isSiteOwner}=this.props.auth;
        const {loading,notice,noticekey}=this.state
        if(!isSiteOwner)
        {
            return(
                <User auth={this.props.auth}/>
            );
		}
        if(loading)
        {
            return(
                <BaseLayout {...this.props.auth}
                title="Exam Arena">
                    <div className="loading-div">
                        <p className="loading-title">Wait a while......</p>
                    </div>
              </BaseLayout>	
            );
        }
		return(
			<BaseLayout {...this.props.auth}
			title="Exam Arena admin-allquestions">
			<div className="base-card">
			<div className="wbjee-jeemain">
            <div style={{flexDirection:'column'}}>
				{notice.map((item,index)=>{
                    return (
                        <div>
                        <Notice key={index} title={index} id={noticekey[index]} text={item.data.noticeText} docs={item.data.noticeimage} date={item.data.date} />
                        <br/>
                        </div>
                    )
                })}
                </div>
			</div>
			</div>
		  </BaseLayout>		  
		);
	}
}
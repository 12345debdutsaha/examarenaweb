import React from 'react';
import BaseLayout from '../components/layout/BaseLayout';
import User from '../components/user';
import { fire } from '../firebase/firebase';
import swal from 'sweetalert';
import DetailCard from '../components/layout/detailcard';
import {Button} from 'reactstrap';
var secretKey="Papadada@983";
const CryptoJS=require('crypto-js');
export default class QusetionDashBoard extends React.Component{
	constructor(props){
		super(props)
		this.state={
			signup:false,
			wbjee:[],
			jeemain:[],
			loading:true,
			showwbjee:true,
			showjeemain:false,
			wbjeekey:[],
			jeemainkey:[]
		}
		this.onSignUpToggle=this.onSignUpToggle.bind(this);
		this.request=this.request.bind(this)
	}
	onSignUpToggle(){
		this.setState({signup:!this.state.signup});
	}
	async request()
	{
		await fire.database().ref('/questionswbjee').once('value')
		.then(async(snap)=>{
			let wbjee=snap.val()
			var i;
			for(i in wbjee)
        {
			this.state.wbjeekey.push(i)
            var bytesText  = await CryptoJS.AES.decrypt(wbjee[i].questionText.toString(), secretKey);
            wbjee[i].questionText=await bytesText.toString(CryptoJS.enc.Utf8);
            var bytesImage  = await CryptoJS.AES.decrypt(wbjee[i].questionimage.toString(), secretKey);
            wbjee[i].questionimage=await bytesImage.toString(CryptoJS.enc.Utf8);
            var bytesCorrect  =await CryptoJS.AES.decrypt(wbjee[i].correct.toString(), secretKey);
            wbjee[i].correct=await bytesCorrect.toString(CryptoJS.enc.Utf8);
        }
			this.setState({wbjee:wbjee})
		}).catch((err)=>{
			swal({
				title:'Error',
				text:`${err.message}`,
				button:'Cancel!!',
				icon:'error'
			})
		})
		await fire.database().ref('/questionsjeemain').once('value')
		.then(async(snap)=>{
			let jeemain=snap.val()
			for(let i in jeemain)
        {
			this.state.jeemainkey.push(i)
            var bytesText  = await CryptoJS.AES.decrypt(jeemain[i].questionText.toString(), secretKey);
            jeemain[i].questionText=await bytesText.toString(CryptoJS.enc.Utf8);
            var bytesImage  = await CryptoJS.AES.decrypt(jeemain[i].questionimage.toString(), secretKey);
            jeemain[i].questionimage=await bytesImage.toString(CryptoJS.enc.Utf8);
            var bytesCorrect  =await CryptoJS.AES.decrypt(jeemain[i].correct.toString(), secretKey);
            jeemain[i].correct=await bytesCorrect.toString(CryptoJS.enc.Utf8);
        }
			this.setState({jeemain:jeemain})
		}).catch((err)=>{
			swal({
				title:'Error',
				text:`${err.message}`,
				button:'Cancel!!',
				icon:'error'
			})
		})
		this.setState({loading:false})
	}
	async componentWillMount(){
		this.request()
	}
	
	render()
	{
		const {isSiteOwner}=this.props.auth;
		const {loading}=this.state;
		const {wbjee,jeemain,showjeemain,showwbjee,wbjeekey,jeemainkey}=this.state;
        if(!isSiteOwner)
        {
            return(
                <User auth={this.props.auth}/>
            );
		}
        if(loading)
        {
            return(
                <BaseLayout {...this.props.auth}
                title="Exam Arena">
                    <div className="loading-div">
                        <p className="loading-title">Wait a while......</p>
                    </div>
              </BaseLayout>	
            );
        }
		return(
			<BaseLayout {...this.props.auth}
			title="Exam Arena admin-allquestions">
			<div className="base-card">
			<div className="wbjee-jeemain">
				<div className={showwbjee?"active1":"inactive1"} onClick={()=>{this.setState({showwbjee:!showwbjee})}}>
				<p>Wbjee Questions</p>
				</div>
				<div className={showjeemain?"active1":"inactive1"} onClick={()=>{this.setState({showjeemain:!showjeemain})}}>
				<p>Jeemain Questions</p>
				</div>
			</div>
			{
				showwbjee && wbjee &&
				wbjeekey.map((item,index)=>{
					return (
					<div className="cards-style" key={index}>
						<DetailCard key={index} index={index} type="wb" id={item} questionText={wbjee[item].questionText} questionImage={wbjee[item].questionimage} Correct={wbjee[item].correct}/>
					</div>
					);
				})
			}
			{
				showjeemain && jeemain &&
				jeemainkey.map((item,index)=>{
					return (
					<div className="cards-style" key={index}>
						<DetailCard key={index} index={index} type="je" id={item} questionText={jeemain[item].questionText} questionImage={jeemain[item].questionimage} Correct={jeemain[item].correct}/>
					</div>
					);
				})
			}
			</div>
		  </BaseLayout>		  
		);
	}
}
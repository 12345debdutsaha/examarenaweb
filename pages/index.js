import React from 'react';
import BaseLayout from '../components/layout/BaseLayout';
import Home from '../components/layout/home';
import { fire } from '../firebase/firebase';
import * as Cookie from 'js-cookie';
export default class Index extends React.Component{
	constructor(props){
		super(props)
		this.state={
			signup:false,
			notice:[],
			loading:true
		}
		this.onSignUpToggle=this.onSignUpToggle.bind(this);
	}
	onSignUpToggle(){
		this.setState({signup:!this.state.signup});
	}
	componentWillMount(){
		try{
		let token=Cookie.get('jwt')
		if(token.startsWith('e'))
		{
			fire.database().ref('/noticeboard').once('value')
			.then(snap=>{
				let notice=snap.val()
				let demo=[]
				for(let i in notice)
				{
					demo.push(notice[i])
				}
				this.setState({notice:demo})
				this.setState({loading:false})
			}).catch((err)=>{
			console.log(err.message)
			})
		}
		else{
			this.setState({loading:false})
		}
		
	}catch(err)
	{
		this.setState({loading:false})
	}
	}
	render()
	{
		const {isAuthenticated}=this.props.auth;
		const {loading,notice}=this.state;
		return(
			<BaseLayout {...this.props.auth}
			title="Exam Arena Home Page">
				<div>
				{!loading? <Home isAuthenticated={isAuthenticated} notice={notice}/>: <h2 
				style={{paddingTop:150,marginLeft:40,color:'white',fontSize:40}}>Wait a while....</h2>}
				</div>
		  </BaseLayout>		  
		);
	}
}
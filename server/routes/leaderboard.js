const express=require('express')

const Router=express.Router()
const LeaderBoard=require('../models/leaderboard')
const mongoose=require('mongoose')
Router.post('/create',async(req,res)=>{
    try{
    let uid=req.body.uid
    let score=req.body.score
    let email=req.body.email
    let catagory=req.body.catagory
    await LeaderBoard.findOne({uid:uid,catagory:catagory}).then(async response=>{
        if(response)
        {
            res.status(200).send({
                message:"Already registered"
            })
        }else{
            const leaderboard=new LeaderBoard({
                _id:mongoose.Types.ObjectId(),
                uid,
                score,
                email,
                catagory
            })
            await leaderboard.save().then(response=>{
                res.status(200).send({
                    message:"Successfully created"
                })
            }).catch(err=>{
                res.status(401).send({
                    message:err.message
                })
            })
        }
    }).catch(err=>{
        res.status(401).send({
            message:err.message
        })
    })
}catch(err)
{
    res.status(401).send({
        message:err.message
    })
}
})


//getting the leaderboard details of wbjee
Router.get('/details/wbjee',async(req,res)=>{
    try{

    await LeaderBoard.find({catagory:'wbjee'})
    .sort({score:-1})
    .then(response=>{
        if(response.length>0)
        {
            res.status(200).send({
                message:"Success",
                data:response
            })
        }else{
            res.status(401).send({
                message:"Empty",
                data:[]
            })
        }
    }).catch(err=>{
        res.status(401).send({
            data:[],
            message:err.message
        })
    })
}catch(err)
{
    res.status(401).send({
        data:[],
        message:err.message
    })
}
})

//getting the leaderboard details of jeemain
Router.get('/details/jeemain',async(req,res)=>{
    try{

    await LeaderBoard.find({catagory:'jeemain'})
    .sort({score:-1})
    .then(response=>{
        if(response.length>0)
        {
            res.status(200).send({
                message:"Success",
                data:response
            })
        }else{
            res.status(401).send({
                message:"Empty",
                data:[]
            })
        }
    }).catch(err=>{
        res.status(401).send({
            data:[],
            message:err.message
        })
    })
}catch(err)
{
    res.status(401).send({
        data:[],
        message:err.message
    })
}
})
//delete all the documents
Router.delete('/all',(req,res)=>{
    LeaderBoard.deleteMany({}).then(response=>{
        res.status(200).send({
            message:"Successfully deleted"
        })
    }).catch(err=>{
        res.status(401).send({
            message:err.message
        })
    })
})
//delete all the wbjee documents
Router.delete('/wbjee',(req,res)=>{
    LeaderBoard.deleteMany({catagory:'wbjee'}).then(response=>{
        res.status(200).send({
            message:"Successfully deleted"
        })
    }).catch(err=>{
        res.status(401).send({
            message:err.message
        })
    })
})

//delete all the jeemain documents
Router.delete('/jeemain',(req,res)=>{
    LeaderBoard.deleteMany({catagory:'jeemain'}).then(response=>{
        res.status(200).send({
            message:"Successfully deleted"
        })
    }).catch(err=>{
        res.status(401).send({
            message:err.message
        })
    })
})

module.exports=Router
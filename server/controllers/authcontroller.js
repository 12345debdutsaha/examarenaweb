const admin=require('../admin/admin');
const CrypoJs=require('crypto-js');
var userSecret="Papadada@983"
exports.retrieveData=(req,res)=>{
    admin.appdatabase.ref('/questionswbjee').once('value').then((snap)=>{
        const data=snap.val();
        res.status(200).send(data);
    }).catch((err)=>{
        res.status(500).send(err.message)
    })
}
exports.retrieveDatajeemain=(req,res)=>{
    admin.appdatabase.ref('/questionsjeemain').once('value').then((snap)=>{
        const data=snap.val();
        res.status(200).send(data);
    }).catch((err)=>{
        res.status(500).send(err.message)
    })
}

exports.userData=async(req,res)=>{
    var pageToken=req.query.pageToken;
    await admin.appauth.listUsers(10,pageToken)
    .then(async(listUserResult)=>{
        let list=await CrypoJs.AES.encrypt(JSON.stringify(listUserResult),userSecret).toString()
        res.status(200).send(list)
    }).catch((err)=>{
        res.status(501).send({
            message:err.message
        })
    })
}

exports.deleteUser=async(req,res)=>{
    let uid=req.params.id;
    let message=await admin.appauth.deleteUser(uid)
    .then((response)=>{
        return {message:"successfully deleted"}
    }).catch((err)=>{
        return {message:"error occured in server"}
    })
    if(message.message==="successfully deleted")
    {
        await admin.appdatabase.ref(`/users/${uid}`).remove()
        .then(()=>{
            res.status(200).send({
                message:"successfully deleted"
            })
        }).catch((err)=>{
            res.status(500).send({
                message:"error occured in server"
            })
        })
    }else
    {
        res.status(500).send({
            message:"error occured in server"
        })
    }
}
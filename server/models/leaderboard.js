const mongoose=require('mongoose')

const LeaderBoard=new mongoose.Schema({
    _id:{type:mongoose.Schema.Types.ObjectId},
    score:{type:Number,required:true},
    email:{type:String,required:true},
    uid:{type:String,required:true},
    catagory:{type:String,required:true}
})
module.exports=mongoose.model('Leaderboard',LeaderBoard)
